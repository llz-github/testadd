package com.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpServer {

    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(8080);
            String m = "";
            Socket socket = ss.accept();
            BufferedReader bd = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            /**
             * 接受HTTP请求，并解析数据
             */
            String requestHeader = "";
            int contentLength = 0;
            while ((requestHeader = bd.readLine()) != null && !requestHeader.isEmpty()) {
                /**
                 * 获得GET参数
                 */
                if (requestHeader.startsWith("GET")) {
                    int a = requestHeader.indexOf("a=") + 2 ;
                    int b = requestHeader.indexOf("b=") + 2;
                    int c = requestHeader.indexOf("/") + 1;
                    int d = requestHeader.indexOf("?");
                    int e = requestHeader.indexOf("HTTP/1.1");

                    String na = requestHeader.substring(a,b-3);
                    String nb = requestHeader.substring(b,e-1);
                    String cal = requestHeader.substring(c, d);

                    a = Integer.parseInt(na);
                    b = Integer.parseInt(nb);

                    if(cal.equals("add")) {
                        m = a + b + "";
                        System.out.println("a" + "+" + "b" + "=" + m);
                    }
                    else if(cal.equals("mult")) {
                        m = a * b + "";
                        System.out.println("a" + "*" + "b" + "=" + m);
                    }
                    else m = "URL ERROR";
                }
            }
            /*发送回执*/
            PrintWriter pw = new PrintWriter(socket.getOutputStream());

            pw.println("HTTP/1.1 200 OK");
            pw.println("Content-type:text/html");
            pw.println();
            pw.println("<h1>" + m + "</h1>");

            pw.flush();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}